# R3C Date #

Biblioteca de funções para manipulação de datas das bibliotecas R3C\Facebook e R3C\Youtube

Recebe nos parametros de todas as funções as datas no padrão recebido pelas API do Facebook e Youtube, e tranforma em um Object DateTime.

## Namespace ##
R3C\FormatDate

# Classes #

## Format Date ##
Classe com todas as funções de manipulação de data necessárias.

### Propriedades ###

### Funções ###

* formatDateFacebookPublished - (static)
    * Parâmetro: 
        * String - Data no formato enviado pela API do Facebook
    * Recebe uma string com a data e a modifica para criar um objeto DateTime.

* formatDateFacebookUpdated - (static)
    * Parâmetro: 
        * String - Data no formato enviado pela API do Facebook
    * Recebe uma string com a data e a modifica para criar um objeto DateTime.
    
* formatDateFacebookCreatedTime - (static)
    * Parâmetro: 
        * String - Data no formato enviado pela API do Facebook
    * Recebe uma string com a data e a modifica para criar um objeto DateTime.
    
* formatDateFacebookUpdatedTime - (static)
    * Parâmetro: 
        * String - Data no formato enviado pela API do Facebook
    * Recebe uma string com a data e a modifica para criar um objeto DateTime.
    
* formatDateYoutube - (static)
    * Parâmetro: 
        * String - Data no formato enviado pela API do Youtube
    * Recebe uma string com a data e a modifica para criar um objeto DateTime.