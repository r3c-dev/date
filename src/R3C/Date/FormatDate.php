<?php
namespace R3C\Date;

use DateTime;

class FormatDate
{
    public static function formatDateFacebookPublished($date)
    {
        $date = str_replace('T', ' ', $date);
        $date = explode('+', $date)[0];

        $date = DateTime::createFromFormat('Y-m-d H:i:s', $date);
        return $date;
    }

    public static function formatDateFacebookUpdated($date)
    {
        $date = str_replace('T', ' ', $date);
        $date = substr($date, 0, -6);

        $date = DateTime::createFromFormat('Y-m-d H:i:s', $date);
        return $date;
    }

    public static function formatDateFacebookCreatedTime($date)
    {
        $date = str_replace('T', ' ', $date);
        $date = substr($date, 0, -5);

        $date = DateTime::createFromFormat('Y-m-d H:i:s', $date);
        return $date;
    }

    public static function formatDateFacebookUpdatedTime($date)
    {
        $date = str_replace('T', ' ', $date);
        $date = substr($date, 0, -5);

        $date = DateTime::createFromFormat('Y-m-d H:i:s', $date);
        return $date;
    }

    public static function formatDateYoutube($date)
    {
        $letras = ['T', 'Z'];
        $date = str_replace($letras, ' ', $date);
        $date = substr($date, 0, - 5);
        $date = DateTime::createFromFormat('Y-m-d H:i:s', $date);
        return $date;
    }
}